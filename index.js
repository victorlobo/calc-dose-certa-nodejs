const express = require('express'); // Importa classe express
const path = require('path'); // Importa classe path
var bodyParser = require('body-parser'); // Importa classe body-parser
const mysql = require('mysql'); // Importa classe mysql
var sha1 = require('sha1'); // Importa classe sha1


const app = express();
__dirname = path.resolve();
app.listen(process.env.port || 3000); // Roda servidor na porta 3000
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(__dirname + '/')); // Define raiz do projeto para ser entendida pelo front-end

// Cria conexão com banco de dados MySQL
const connection = mysql.createConnection({
	host: 'remotemysql.com',
	user: 'wikiWvOy0W',
	password: 'rA4Lr7ihXH',
	database: 'wikiWvOy0W',
	port: '3306'
});

app.engine('html', require('ejs').renderFile);

// Cria view para apresentar informações na home
app.get('/', (req, res) => {
	res.render(__dirname + '/views/home.html');
});

// Cria view para receber informações enviadas para a home
app.post('/', function(req, res){
	// Recebe informações do formulário
	var usuario = req.body.usuario;
	var senha = sha1(req.body.senha);

	// Executa query para buscar usuário no BD
	connection.query('SELECT * FROM `usuario` WHERE usuario = ? AND senha = ?', [usuario, senha], function(err, rows, fields) {
		// Se conexão falhar
		if (err){
			// Imprime erro no console do servidor
			console.log ('error', err.message, err.stack);
		}
		// Se conexão funcionar
		else{
			// Se encontrou usuário
			if(rows.length > 0){
				// Redireciona o usuário para a página da calculadora
				res.redirect('/calculadora?id_usuario=' + rows[0].id_usuario);
			}
			// Se não encontrou usuário retorna com a mensagem de erro
			else{
				// Renderiza página home apresentando mensagem de erro
				res.render(__dirname + '/views/home.html', {msg: createMessage("danger", "Usuário ou senha inválidos!")});
			}
		}
	});
});

// Cria view para apresentar informações na página calculadora
app.get('/calculadora', (req, res) => {
	// Recebe variáveis passadas pela URL
	var id_usuario = req.query.id_usuario;

	// Executa query para buscar usuário no BD
	connection.query('SELECT nome FROM `usuario` WHERE id_usuario = ?', [id_usuario], function(err, rows, fields) {
		// Se conexão falhar
		if (err){
			// Imprime erro no console do servidor
			console.log ('error', err.message, err.stack)
		}
		// Se conexão funcionar
		else{
			// Renderiza págida calculadora com dados do usuário
			res.render(__dirname + '/views/calculadora.html', {id_usuario: id_usuario, nome: rows[0].nome});
		}
	});
});

// Cria view para receber informações enviadas para a calculadora
app.post('/calculadora', function(req, res){
	// Recebe variáveis passadas pela URL
	var id_usuario = req.query.id_usuario;

	// Executa query para buscar usuário no BD
	connection.query('SELECT nome FROM `usuario` WHERE id_usuario = ?', [id_usuario], function(err, rows, fields) {
		// Se conexão falhar
		if (err){
			// Imprime erro no console do servidor
			console.log ('error', err.message, err.stack)
		}
		// Se conexão funcionar
		else{
			// Recebe variáveis passadas via formulário
			var qa = req.body.qa;
			var d = req.body.d;
			var porcentagem = req.body.porcentagem;
			const c = 10;
			// Realiza cálculo da Dose Certa
			var qs = (qa * d) / (porcentagem * c);

			// Cria vetor com dados da Dose Certa para relatório
			var relatorio = {
				id_usuario: id_usuario,
				qa: qa,
				d: d,
				porcentagem: porcentagem,
				c: c,
				qs: qs,
			}

			// Salva informações do relatório no BD
			connection.query('INSERT INTO relatorio SET ?', relatorio, (err, resp) => {
				// Se conexão falhar
				if (err){
					// Imprime erro no console do servidor
					console.log ('error', err.message, err.stack)
				}
				// Se conexão funcionar
				else
					// Imprime último ID cadastrado no console do servidor
					console.log('ID do ultimo inserido:', resp.insertId);
			});
			// Renderiza páginda de resultado da Dose Certa com dados do usuário e do relatório
			res.render(__dirname + '/views/calculadora-resultado.html', {id_usuario: id_usuario, nome: rows[0].nome, relatorio: relatorio});
		}
	});
});

// Cria view para apresentar informações na página relatório
app.get('/relatorio', (req, resposta) => {
	// Recebe variáveis passadas pela URL
	var id_usuario = req.query.id_usuario;
	// Executa query para buscar nome de usuário usuário no BD
	connection.query('SELECT nome FROM `usuario` WHERE id_usuario = ?', [id_usuario], function(err, usuarios, fields) {
		// Se conexão falhar
		if (err){
			// Imprime erro no console do servidor
			console.log ('error', err.message, err.stack)
		}
		// Se conexão funcionar
		else{
			// Executa query para buscar dados do usuário com relatório no BD
			connection.query('SELECT R.*, U.nome as nome_usuario FROM relatorio R JOIN usuario U ON R.id_usuario = U.id_usuario', function(err, relatorios, fields) {
				if (err){
					// Imprime erro no console do servidor
					console.log ('error', err.message, err.stack)
				}
				// Se conexão funcionar
				else{
					// Renderiza páginda de relatório com dados do usuário e do relatório
					resposta.render(__dirname+'/views/relatorio.html', {id_usuario: id_usuario, nome: usuarios[0].nome, relatorios:relatorios});
				}
			});
		}
	});
});

// Cria view para apresentar formulário de cadastro de usuário
app.get('/cadastrar', (req, resposta) => {
	// Renderiza páginda de cadastro de usuário
	resposta.render(__dirname+'/views/cadastrar.html');
});

// Cria view para receber informações enviadas para a página de cadastro de usuário
app.post('/cadastrar', function(request,res){
	// Recebe variáveis passadas pela URL
	var login = request.body.usuario;
	// Executa query para buscar id do usuário no BD
	connection.query('SELECT id_usuario FROM `usuario` WHERE usuario = ?', [login], function(err, usuarios, fields) {
		// Se conexão falhar
		if (err){
			// Imprime erro no console do servidor
			console.log ('error', err.message, err.stack)
		}
		// Se conexão funcionar
		else{
			// Se não encontrou usuário
			if(usuarios.length <= 0){
				// Cria vetor de variáveis passadas via formulário e via URL
				var usuario = {
					'usuario': login,
					'nome': request.body.nome,
					'email': request.body.email,
					'senha': sha1(request.body.senha)
				};
				// Salva informações do usuário no BD
				connection.query('INSERT INTO usuario SET ?', usuario, (err, resp) => {
					// Se conexão falhar
					if (err){
						// Imprime erro no console do servidor
						console.log ('error', err.message, err.stack)
					}
					// Se conexão funcionar
					else{
						// Imprime último ID cadastrado no console do servidor
						console.log('ID do ultimo inserido:', resp.insertId);
					}
				});
				// Renderiza páginda de login passando mensagem de sucesso no cadastro
				res.render(__dirname + '/views/home.html', {msg: createMessage("success", "Usuário cadastrado com sucesso!")});
			}
			// Se encontrou usuário
			else{
				// Renderiza páginda de cadastro de usuário passando mensagem de erro no cadastro
				res.render(__dirname + '/views/cadastrar.html', {msg: createMessage("danger", "Já existe um usuário com este nome de usuário!")});
			}
		}
	});
});

// Método para criação de mensagem padronizada
function createMessage(type, text)
{
	// Retorna vetor com dados da mensagem
	return {type:type, text:text}
}

// Imprime mensagem no console do servidor para informar que servidor está rodando corretamente
console.log('Running at Port 3000');
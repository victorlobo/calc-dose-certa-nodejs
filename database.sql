CREATE TABLE usuario (
	id_usuario INT NOT NULL AUTO_INCREMENT,
	usuario VARCHAR(40),
	nome VARCHAR(100),
	email VARCHAR(100),
	senha VARCHAR(60),
	PRIMARY KEY(id_usuario)
);

CREATE TABLE relatorio (
	id_relatorio INT NOT NULL AUTO_INCREMENT,
	id_usuario INT NOT NULL,
	qa INT NOT NULL,
	d INT NOT NULL,
	porcentagem INT NOT NULL,
	c INT NOT NULL,
	qs FLOAT NOT NULL,
	PRIMARY KEY(id_relatorio),
	FOREIGN KEY(id_usuario) REFERENCES usuario(id_usuario)
);

INSERT INTO usuario VALUES
	(
		1,
		'victor',
		'Victor Lobo',
		'victor@gmail.com',
		'40bd001563085fc35165329ea1ff5c5ecbdbbeef'
	),
	(
		2,
		'eduardo',
		'Eduardo Mendes',
		'eduardo@gmail.com',
		'40bd001563085fc35165329ea1ff5c5ecbdbbeef'
	)
;